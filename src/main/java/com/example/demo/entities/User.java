package com.example.demo.entities;

import com.example.demo.enumerations.ROLE;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "users")
public class User implements Serializable  {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(nullable = false, unique = true, length = 40)
    private String username;

    @Column(nullable = false, unique = true, length = 70)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private ROLE role = ROLE.USER;
}
