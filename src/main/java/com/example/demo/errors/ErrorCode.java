package com.example.demo.errors;

public enum ErrorCode {
    UNKNOWN, NOT_FOUND, UNAUTHORIZED, NOT_VALID,
}
