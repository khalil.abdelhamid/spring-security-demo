package com.example.demo.errors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

public class RESTGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ErrorMessage> handleNotAllowed(BadRequestException ex) {
        ex.printStackTrace();
        ErrorMessage error = new ErrorMessage(ex.getMessage(), ex.getErrorCode());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> handlerInternalError(Exception ex) {
        ex.printStackTrace();
        ErrorMessage error = new ErrorMessage(ex.getMessage(), ErrorCode.UNKNOWN);
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @ExceptionHandler(Exception.class)
    protected ResponseEntity<ErrorMessage> handleDefault(Exception ex,
                                             HttpHeaders headers, HttpStatus status, WebRequest webRequest) {
       ex.printStackTrace();
       ErrorMessage error = new ErrorMessage(ex.getMessage(), ErrorCode.UNKNOWN);
       return ResponseEntity.status(status).headers(headers).body(error);
    }
}
