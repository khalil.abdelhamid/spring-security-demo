package com.example.demo.errors;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
public class ErrorMessage {

    private String errorMessage;
    private ErrorCode errorCode;
    private String timestamp;

    public ErrorMessage(String errorMessage, ErrorCode errorCode) {
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
        this.timestamp = new Timestamp(new Date().getTime()).toString();
    }
}
