package com.example.demo.controllers;

import com.example.demo.entities.Project;
import com.example.demo.entities.Task;
import com.example.demo.errors.BadRequestException;
import com.example.demo.errors.ErrorCode;
import com.example.demo.repositories.ProjectRepository;
import com.example.demo.repositories.TaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class TaskController {

    Logger logger = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private TaskRepository repository;

    @Autowired
    private ProjectRepository projectRepository;


    @GetMapping("/tasks/{id}")
    public ResponseEntity<Task> getById(@PathVariable("id") UUID id) {
        logger.info("GET request to /tasks with id {}", id);
        Optional<Task> task = repository.findById(id);
        return ResponseEntity.of(task);
    }

    @GetMapping("/projects/{id}/tasks")
    public List<Task> getAll(@PathVariable("id") UUID id) {
        logger.info("GET Request to /tasks");
        Project project = projectRepository.findById(id)
                .orElseThrow(() -> new BadRequestException("Project not found",
                        ErrorCode.NOT_FOUND));

        return project.getTasks();
    }

    @PostMapping("/projects/{id}/tasks")
    public ResponseEntity<Task> createTask(@RequestBody Task task,
                                          @PathVariable UUID id) throws URISyntaxException {
        logger.info("POST request to /tasks");

        Project project = projectRepository.findById(id)
                .orElseThrow(() -> new BadRequestException("Project not found", ErrorCode.NOT_FOUND));

        task.setProject(project);
        repository.save(task);
        return ResponseEntity.created(new URI("/tasks/" + task.getId())).body(task);
    }

    @PutMapping("/tasks/{id}")
    public ResponseEntity<Task> updateTask(@RequestBody Task task,
                                           @PathVariable UUID id) {
        logger.info("PUT request to /tasks");
        logger.debug("Request body: {}", task);

        Task original = repository.findById(id)
                .orElseThrow(() -> new BadRequestException("Task not found", ErrorCode.NOT_FOUND));

        task.setProject(original.getProject());
        repository.save(task);
        return ResponseEntity.ok().body(task);
    }

    @DeleteMapping("/tasks/{id}")
    public ResponseEntity<Void> deleteTask(@PathVariable("id") UUID id) {
        logger.info("DELETE request to /tasks with id {}", id);

        Task task = repository.findById(id)
                .orElseThrow(() -> new BadRequestException("Task not found", ErrorCode.NOT_FOUND));

        repository.delete(task);
        return ResponseEntity.noContent().build();
    }
}
