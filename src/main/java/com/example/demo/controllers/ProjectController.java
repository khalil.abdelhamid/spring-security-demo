package com.example.demo.controllers;

import com.example.demo.entities.Project;
import com.example.demo.errors.BadRequestException;
import com.example.demo.errors.ErrorCode;
import com.example.demo.repositories.ProjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class ProjectController {

    private final Logger logger = LoggerFactory.getLogger(ProjectController.class);
    private final static String ENTITY_NAME = "Project";

    @Autowired
    private ProjectRepository repo;

    @GetMapping("/projects/{id}")
    public ResponseEntity<Project> get(@PathVariable("id") UUID id) {
        logger.info("GET request to /projects with id {}", id);
        Optional<Project> project = repo.findById(id);
        return ResponseEntity.of(project);
    }

    @GetMapping("/projects")
    public List<Project> getAll() {
        logger.info("GET request to /projects");
        return repo.findAll();
    }

    @PostMapping("/projects")
    public ResponseEntity<Project> create(@Validated @RequestBody Project project) throws URISyntaxException {
        logger.info("POST request to /projects");
        logger.info("Request body {}", project);

        if (project.getId() != null) {
            throw new BadRequestException("Project should NOT have an id",
                    ErrorCode.NOT_VALID);
        }

        Project saved = repo.save(project);
        return ResponseEntity.created(new URI("/api/v1/projects/" + saved.getId()))
                .body(saved);
    }

    @PutMapping("/projects/{id}")
    public ResponseEntity<Project> update(@RequestBody Project project,
                                          @PathVariable UUID id) {
        logger.info("PUT request to /projects");
        logger.debug("Request body {}", project);

        Project originalProject = repo.findById(id)
                .orElseThrow(() -> new BadRequestException("Project not found",
                        ErrorCode.NOT_FOUND));

        project.setId(originalProject.getId());
        repo.save(project);
        return ResponseEntity.ok().body(project);
    }

    @DeleteMapping("/projects/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") UUID id) {
        logger.info("DELETE request to /projects with id {}", id);

        Project found = repo.findById(id)
                .orElseThrow(() -> new BadRequestException("Project not found",
                    ErrorCode.NOT_FOUND));
        repo.delete(found);
        return ResponseEntity.noContent().build();
    }
}
